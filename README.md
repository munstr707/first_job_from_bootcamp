# First Job from Bootcamp

## Motivation

While speaking with serveral students after class the other faculty and I realized we had covered a large amount of useful material that could help you in preparing and setting up interviews after during the final days of class and beyond.

## Getting the first interviews

### Advertise your strongest assets

Your projects, assignments, and every line of code you have written in class and outside of class are your strongest assets for selling yourself. They are the validation of your claims of what you know and also examples of your efforts. Having all these projects available on either GitHub or GitLab is essential. Even more so you have to make them visible to possible employers. Do your best to link your github and gitlab profiles with linkedin and other places where recruiters and companies will be looking to see your skills.

This can be come even more important during the interview process where a strong github and gitlab presence of side projects and work completed while you were learning can even land you a much easier interview because potential employers have already seen a demonstration of your skills.

Even as you move into the future, your available projects online will always be essential for advertising yourself. You never know when an employer or recruiter will catch something you did on GitHub or Gitlab and then proceed to help set up an interview for you.

As a general rule of thumb, all code you write while learning or working on a side project - no matter how large or small - should have a place on github or gitlab.

## Interview Practice

Interviews are likely going to cover both you personally and technically, thus its important to start thinking about how you can practice discussing both with an interviewer or recruiter. Research technical JavaScript questions, algorithms, and personal questions online.

### Personal Questions

These questions are to get a feel for you both as a person and as a developer. They are generally less concerned with deep technical details, but providing technical depth to your answers here can help make you stand out.

Be sure to prepare for questions such as:

* Give an example of a time you had a disagreement with a coworker and explain how you resolved it.
* Give a time that you struggled and maybe even failed while developing something and how you overcame it.
* Why should we choose you?

All these questions are tough and generally require some forethought to appropriately answer.

### Technical Questions

Inevitably you will be answering technical questions regarding algorithms, problem solving, languages, and libraries. They are doing this to feel out how you think and work through a problem. Even when you do get asked that ridiculous algorithm question that does not seem applicable to anything you normally do, its suppossed to be. They want to see how you handle a challenging problem and work through it.

#### The point is often not the solution, but your process of arriving there

I have personally failed to finish a proper solution for a complex algorithm before, but still succeeded wonderfully because of working through the problem and keeping momentum.

#### Momentum

You will eventually be asked a problem which has you stuck whether temporarily or entirely, the point is to keep moving with it. Do your best to talk about and describe the problem as you work on it. Take the tiniest steps at a time if you are stuck with it and try not to let the larger problem overwhelm you. Even at this point if you are totally lost you can end up walking yourself to a solution if you just try small step by small step. Even if never arriving you can still "succeed" by doing well under the pressure and doing your best to step through it.

Solving the question or problem is always ideal, but doing your best to get there is the next best.

#### Technical Questions for Practice

Below is a list of technical questions that we came up with. THIS IS NOT AN EXHAUSTIVE LIST! The list is enough to get you started and hopefully help you identify some gaps in your knowledge that you will need to cover a little better as you prepare. Do some research online and see what else you can find to help you study.

#### Sample Technical Questions

* How would you rate your proficiency in JavaScript?
* What are some differences between ES5 and ES6?
* What is the difference between a functional component and class component in React?
  * When would you use one vs the other?
  * What is a key difference between the two?
* In NodeJS if you console.log the window object what would it output?
* What is component based development/architecture.
* What is the difference between a Callback and a Promise.
* When you have a bug in a MERN website, explain your process for debugging it. For example, let's say a page you expect to work is completely white.
* Imagine you have a MySQL database table of customers, with columns such has first and last name, address, contact information, etc.
  * Using pseudocode write a query that selects customer's email address whose firstName starts with the letter J.
  * On the same table select all customers who's phone number is NULL
* Using pseudocode write a JavaScript function that checks if a parameter is an Integer.
* What is JSX?
* In JavaScript what is the difference between double equals and triple equals: == vs ===
* What is an ORM?
* Explain a inner join.
* Using pseudocode write a JavaScript function that takes a string and returns the reverse of it.
* In HTML whats the difference between a class and an ID?
  * When would you use one vs the other.

## Handling the first Interview

Make sure that you have prepared for some of the common personal questions and have brushed up on the technical questions. There are a large range of technical problems you could be asked so its less about knowing the answer to any SPECIFIC problem and more knowing the proccesses and various methods which arise to a solution. Make sure to have worked on filling any gaps in your knowledge.

### The interview process

The interview will usually at this stage be in two parts.

1.  Phone interview
2.  On-site interview

The phone interview is generally a softer interview where you could be asked some simple technical questions, but often you are asked mostly personal questions about who you are and your work.

#### Make sure to be ready to be talk about your work

Your technical interview will follow given you did well enough on the previous phone interview and you will be brought on site to meet with others in person and test your technical knowledge. You generally meet with several different people here, all of which will have technical questions or want to talk to you about your work and ask more personal questions.

Whenever solving technical problems make sure to keep moving and not motionless for too long. Its totally fine and acceptable to ask for clarification where necessary. While solving the problem be sure to express your thoughts as you continue so the interviewer can see how you think through the problem. Remember that frequently it is as much about the process as it is about solving the problem and that you are working towards a reasonably correct solution.
